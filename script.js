"use strict";

const button = document.querySelector('.btn');
const userIpData = document.querySelector('.data');

async function getUserIP(ip) {
	const response = await fetch(ip);
	return await response.json();
}

async function findUserByIP() {
	const ipData = await getUserIP('http://api.ipify.org/?format=json');

	const RetrievedData = await getUserIP(`http://ip-api.com/json/${ipData.ip}?fields=45674495`);
	const IP = RetrievedData.query;
	const continent = RetrievedData.continent;
	const country = RetrievedData.country;
	const region = RetrievedData.region;
	const city = RetrievedData.city;
	const district = RetrievedData.regionName;
	const zipCode = RetrievedData.zip;
	const currency = RetrievedData.currency;

	userIpData.innerHTML = `
		IP: ${IP}
        <br> Континент: ${continent}
        <br> Країна: ${country}
        <br> Регіон: ${region}
        <br> Місто: ${city}
        <br> Район: ${district}
		<br> Поштовий індекс: ${zipCode}
		<br> Гроші в обігу: ${currency}`
};


button.addEventListener('click', () => {
	findUserByIP();
});